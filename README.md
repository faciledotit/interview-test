# Interview test

Hello Hacker!

Grazie per aver accettato la nostra intervista tecnica.

In questo breve documento troverai la descrizione di un problema di business da risolvere attraverso la modellazione
di un sistema software.

La descrizione del contesto è sommaria e non esausitiva, _sentiti pure libero di fare delle assunzioni se necessario_, ma
ricordati di documentarle.

Il sistema software dovrà essere scritto utilizzando uno dei seguenti linguaggi a tua scelta:

- PHP 7.4+
- JavaScript ECMAScript 2015 (ES6) +
- TypeScript 3.8+

Ti invitiamo a fornire un breve README che descriva le modalità di utilizzo ed esecuzione del software, 
in modo da rendere agevole la valutazione.

E' un plus se:

- Descrivi brevemente l'architettura del software
- Scrivi i test automatici

## Introduzione

Lo sviluppo di interfaccia web o CLI per l'utilizzo del software è opzionale.

Non è necessario utilizzare un sistema di persistenza dei dati.

Ti invitiamo a concentrarti sul design della business logic.

Il software da sviluppare ha un solo requisito, definito formalmente al termine dell'introduzione 
del contesto e di un esempio.

## Contesto

L'azienda _Carousel_ è l'azienda leader in Italia nel settore delle giostre per bambini. 
Essa dispone di una rete di circa 2.500 giostrai sul territorio nazionale che si fregiano del marchio Carousel, 
noto come sinonimo di serietà e sicurezza.

L'azienda riconosce ad ogni giostraio dei compensi **mensili** sulla base dei seguenti tre parametri di performance:

- **Numero di gettoni venduti**
- **Numero di giri di giostra**
- **Prezzo medio del singolo gettone**

I compensi sono pagati in modo differente a seconda delle soglie raggiunte dai giostrai, dette target.

Alla luce del numero elevato di giostrai, Carousel vuole implementare un sistema software per automatizzare il calcolo 
mensile dei compensi per la propria rete.

Di seguito verranno approfondite le modalità di definizione dei target e un esempio di calcolo del compenso.

### Configurazioni e target

Le soglie target possono essere definite con cadenza mensile e hanno un mese di inizio validità,
a partire dal quale saranno applicate.

Quelle che seguono sono due tabelle che riassumono per il mese di novembre 2018 i target stabiliti da Carousel.

| Parametro                        | Soglia 1 | Soglia 2 | Soglia 3 | Soglia 4 |
| -------------------------------- | -------- | -------- | -------- | -------- |
| Numero di gettoni venduti        | 100      | 180      | 300      | 500      |
| Numero di giri di giostra        | 200      | 400      | 750      | 1500     |
| Prezzo medio del singolo gettone | 1.00     | 1.10     | 1.20     | 1.50     |


| Compensi                         | Soglia 1 | Soglia 2 | Soglia 3 | Soglia 4 |
| -------------------------------- | -------- | -------- | -------- | -------- |
| Compenso per gettone venduto     | 0.10     | 0.12     | 0.30     | 0.50     |

Una definizione di soglie target viene chiamata **configurazione** ed è definita da un oggetto JSON.

Quella che segue è la configurazione delle soglie target dei compensi valida a partire dal mese di novembre 2018.

```javascript
{  
   "startDate": "2018-11-01",
   "targets": [  
      {  
         "tokensSold": 100,
         "carouselRides": 200,
         "avgTokenPrice": 1.00,
         "recompenseForTokenSold": 0.10
      },
      {  
         "tokensSold": 180,
         "carouselRides": 400,
         "avgTokenPrice": 1.10,
         "recompenseForTokenSold": 0.12
      },
      {  
         "tokensSold": 300,
         "carouselRides": 750,
         "avgTokenPrice": 1.20,
         "recompenseForTokenSold": 0.30
      },
      {  
         "tokensSold": 500,
         "carouselRides": 1500,
         "avgTokenPrice": 1.50,
         "recompenseForTokenSold": 0.50
      }
   ]
}
```

L'oggetto configurazione è formato dai seguenti fields:

| Field     | Data type      | Description                                                                                                                                                                                     |
| --------- | -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| startDate | Date           | La data indica il **mese** e l'**anno** di inizio validità della configurazione. E' sempre nel formato Y-m-d e **il giorno è sempre 1**, poichè le configurazioni hanno sempre validità mensile |
| targets   | Array<Target>  | E' un array di **Target**                                                                                                                                                                       |

Ogni **Target** è un oggetto formato dai seguenti fields:

| Field                  | Data type      | Description                                                                                    |
| ---------------------- | -------------- | ---------------------------------------------------------------------------------------------- |
| tokensSold             | Integer        | Indica il numero minimo di gettoni venduti nel mese                                            |
| carouselRides          | Integer        | Indica il numero minimo di giri di giostra nel mese                                            |
| avgTokenPrice          | Float          | Indica il minimo prezzo medio applicato nel mese                                               |
| recompenseForTokenSold | Float          | Indica l'importo in euro del compenso pagato al giostraio per singolo gettone venduto nel mese |

### Calcolo del compenso

Per spiegare il calcolo del compenso mensile utilizzeremo un esempio pratico: il caso della giostraia Manuela.

Nel mese di novembre 2018, la giostraia Manuela, tra le più virtuose dell'intera rete di Carousel, 
ha venduto 511 gettoni ad un prezzo medio di 1.30 euro effettuando 1.244 giri di giostra.

Potremmo rappresentrare le performance del mese di novembre di Manuela con l'oggetto JSON seguente:

```javascript
{  
   "tokensSold": 511,
   "carouselRides": 1244,
   "avgTokenPrice": 1.30
}
```

Considerando la configurazione di cui prima, la soglia target di riferimento di Manuela è la seguente:

```javascript
{  
   "tokensSold": 300,
   "carouselRides": 750,
   "avgTokenPrice": 1.20,
   "recompenseForTokenSold": 0.30
}
```

Manuela non ha raggiunto la soglia target 4, poichè avrebbe dovuto raggiungere il numero minimo di giri di 
giostra (1.500), è quindi in soglia 3.

Per il mese di novembre 2018, Manuela riceverà come compenso 153.30 euro (`511 * 0.30`).

### Requisiti del software

Il requisito del software è descritto dalla seguente [User Story](https://en.wikipedia.org/wiki/User_story):

_Come azienda Carousel, vorrei calcolare il compenso mensile dei giostrai della mia rete sulla base di configurazioni 
basate su target, così posso pagare i giostrai sulla base delle loro performance_

### Note

- Per mese di validità si intende sempre il mese solare
- Non possono esistere due configurazioni dei target per lo stesso mese

### Consegna del test e domande

La consegna del test deve essere effettuata attraverso l'invio dei riferimenti al repository GIT (Bitbucket, GitLab, GitHub, etc.) con la soluzione
all'indirizzo e-mail [salvatore.cordiano+interview@facile.it](mailto:salvatore.cordiano+interview@facile.it).

Al predetto indirizzo e-mail siamo a disposizione per domande e dubbi sul test.